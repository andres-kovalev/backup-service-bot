const fetch = require('node-fetch');
const fs = require('fs');
const { exec } = require('child_process');
const CREDENTIALS = require('./credentials.json');

const BACKUP_DIR = './backup/';
const UPLOAD_API_URL = 'https://bekky.akovalev.ru/api/upload?file=';

backup();

async function backup() {
    const backupFileName = generateBackupFileName();
    const backupLocalFileName = `${BACKUP_DIR}${backupFileName}`;

    await createBackup(CREDENTIALS.db, CREDENTIALS.user, CREDENTIALS.password, backupLocalFileName);
    await upload(backupLocalFileName, backupFileName);
}

function generateBackupFileName() {
    const now = new Date();

    const timestamp = [
        now.getFullYear(),
        now.getMonth() + 1,
        now.getDate(),
        now.getHours(),
        now.getMinutes(),
        now.getSeconds(),
    ]
        .map(item => item.toString().padStart(2, '0'))
        .join('');

    return `${timestamp}.sql.gz`;
}

async function createBackup(db, user, password, path) {
    await execute(`mysqldump ${db} -u ${user} -p${password} | gzip > ${path}`);
}

function execute(command) {
    return new Promise((resolve, reject) => {
        exec(command, (error, stdout) => {
            // eslint-disable-next-line no-unused-expressions
            error
                ? reject(error)
                : resolve(stdout);
        });
    });
}

async function upload(file, path) {
    const stats = fs.statSync(file);
    const fileSizeInBytes = stats.size;
    const readStream = fs.createReadStream(file);

    await fetch(`${UPLOAD_API_URL}${path}`, {
        method: 'PUT',
        headers: {
            'Authorization': `Bearer ${CREDENTIALS.service_token}`,
            "Content-length": fileSizeInBytes,
        },
        body: readStream,
    });
}